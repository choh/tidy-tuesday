# Tidy Tuesday

This is a collection of mini-projects as part of the 
[Tidy Tuesday](https://github.com/rfordatascience/tidytuesday) series of 
weekly data sets aimed at practicing data wrangling and visualisation. 

## Code

All code is in the `R` directory, with the source files being named so they
can be associated with a year and week. The source files aim to be 
self-contained, so, given all dependencies are installed in your system, they 
can be run and then do everything needed from fetching the data to saving
plots. 

My aim with these projects is to see whether I can generate some nice 
visualisation or summary of the data in a short amount of time.
I aim to spend less than 90 minutes on each dataset, so the visualisations 
might not be perfect. 

## Data

The raw data comes from various places and is usually hosted on the 
[Tidy Tuesday Github page](https://github.com/rfordatascience/tidytuesday). 
The scripts will attempt to download the files and put them into a `data-raw`
directory (if they are not already in the expected location). 

## Licence 

All contents of this repository are available under CC BY-NC-SA 4.0, see file
LICENCE.
