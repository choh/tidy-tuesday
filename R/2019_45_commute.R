# ----- load packages -----
library("dplyr")
library("ggplot2")

# --- furhter dependencies are here, readr, rnaturalearth, ggiraph

# ----- path setup -----
mydir <- list()
mydir$base <- here::here()
mydir$raw <- file.path(mydir$base, "data-raw")
mydir$plots <- file.path(mydir$base, "plots")

if (!dir.exists(mydir$raw)) {
    dir.create(mydir$raw, mode = "0755")
}

if (!dir.exists(mydir$plots)) {
    dir.create(mydir$plots, mode = "0755")
}

# ----- read data -----
# use the clean version from the repo, download if it does not exist
raw_file <- file.path(mydir$raw, "commute.csv")
if (!file.exists(raw_file)) {
    message("Downloading raw data as it does not exist locally.")
    download.file(paste0("https://raw.githubusercontent.com/rfordatascience/",
                         "tidytuesday/master/data/2019/2019-11-05/commute.csv"),
                  destfile = raw_file)
}
commute <- readr::read_csv(raw_file)

# need to fix some names
commute$state <- recode(commute$state,
                        "Ca" = "California",
                        "Massachusett" = "Massachusetts")

avg_mode <- commute %>%
    group_by(state, mode) %>%
    summarise(mean = mean(percent))

# ----- get map data -----
us_states <- rnaturalearth::ne_states(country = "united states of america",
                                      returnclass = "sf")
# add background map data
# there are two layers: one additional map of the us to show bodies of water,
# and another for the rest of north america for context
na_map_data <- rnaturalearth::ne_countries(
    continent = "north america", scale = 10, returnclass = "sf")
na_map <- na_map_data %>% filter(name != "United States of America")
us_bg_map <- na_map_data %>%  filter(name == "United States of America")

# ----- combine map data and commute data ------
us_states <- full_join(us_states, avg_mode, by = c("name" = "state"))
walk_commute <- us_states %>% filter(mode == "Walk")
bike_commute <- us_states %>% filter(mode == "Bike")

# ----- make maps ------
# as we have to create multiple similar plots, create a function
# outside of a tidy tuesday _mini project_ this should be put in a separate file
commute_map <- function(data, highcolour) {
    map_base <- ggplot() +
        geom_sf(data = na_map, fill = "antiquewhite1", colour = NA) +
        geom_sf(data = us_bg_map, fill = "aliceblue", colour = "aliceblue") +
        ggiraph::geom_sf_interactive(data = {{ data }}, colour = "grey70",
            aes(fill = mean,
                tooltip = paste0(name, ": ", round(mean, 2), "%"))) +
        coord_sf(datum = NA) +
        scale_fill_gradient(low = "white", high = highcolour) +
        theme(panel.background = ggplot2::element_rect(fill = "aliceblue"),
              legend.position = "bottom",
              text = element_text(size = 20))

    map_us_core <- map_base +
        xlim(c(-130, -65)) +
        ylim(c(23, 50))

    # create submaps with the respective limits, omit guides as otherwise
    # these will be in the subplots as well
    map_alaska <- map_base +
        xlim(c(-180, -130)) +
        ylim(c(50, 72)) +
        guides(fill = FALSE)

    map_hawaii <- map_base +
        xlim(c(-162, -154)) +
        ylim(c(18, 24)) +
        guides(fill = FALSE)

    full_map <- map_us_core +
        annotation_custom(ggplotGrob(map_alaska), xmin = -133, xmax = -120,
                          ymax = 32) +
        annotation_custom(ggplotGrob(map_hawaii), xmin = -119, xmax = -107,
                          ymax = 32)
    full_map
}

# make the plots interactive by calling ggiraph::girafe(ggobj = plot)
walk_map <- commute_map(walk_commute, highcolour = "green4") +
    labs(title = "Percentage of people walking to work in cities in the US",
         subtitle = "Data from cities is aggregated per state",
         fill = "Percentage")

bike_map <- commute_map(bike_commute, highcolour = "blue4") +
    labs(title = "Percentage of people biking to work in cities in the US",
         subtitle = "Data from cities is aggregated per state",
         fill = "Percentage")

# ----- save plots to disk ------
ggsave(file.path(mydir$plots, "2019_45_walk_map.png"), walk_map,
       width = 15, height = 10)
ggsave(file.path(mydir$plots, "2019_45_bike_map.png"), bike_map,
       width = 15, height = 10)
